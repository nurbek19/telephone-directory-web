import React from 'react';
import './ModalContent.css';

const ModalContent = props => {
    return(
        <div className="modal-container">
            <span className="close-button" onClick={props.closed}>Close</span>

            <div className="modal-content">
                <img src={props.img} alt="contact"/>

                <div className="about-contact">
                    <h1>{props.name}</h1>
                    <p>{props.phone}</p>
                    <p>{props.email}</p>
                </div>

                <div className="buttons">
                    <button onClick={props.edit}>Edit</button>
                    <button onClick={props.remove}>Delete</button>
                </div>
            </div>
        </div>
    )
};

export default ModalContent;