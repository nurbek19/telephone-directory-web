import React from 'react';
import './Contact.css';

const Contact = props => {
  return(
      <div className="contact" onClick={props.showed}>
          <img src={props.img} alt="contact"/>
          <h2>{props.name}</h2>
      </div>
  )
};

export default Contact;