import React from 'react';
import {Link} from 'react-router-dom';
import './ContactForm.css';

const ContactForm = props => {
  return(
      <form action="#" className="contact-form">
          <legend>{props.title}</legend>

          <div className="form-control">
              <label>Name</label>
              <input type="text" name="name" onChange={props.changeValue} value={props.name}/>
          </div>

          <div className="form-control">
              <label>Phone</label>
              <input type="number" name="phone" onChange={props.changeValue} value={props.phone}/>
          </div>

          <div className="form-control">
              <label>Email</label>
              <input type="email" name="email" onChange={props.changeValue} value={props.email}/>
          </div>

          <div className="form-control">
              <label>Photo</label>
              <input type="text" name="photo" onChange={props.changeValue} value={props.photo}/>
          </div>

          <div className="image-preview">
              <p>Photo preview</p>
              <img src={props.preview} alt="preview"/>
          </div>

          <button onClick={props.saved}>Save</button>
          <Link to="/">Back to list</Link>
      </form>
  )
};

export default ContactForm;