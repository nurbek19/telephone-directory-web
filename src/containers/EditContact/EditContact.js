import React, {Component} from 'react';
import {connect} from 'react-redux';

import ContactForm from '../../components/ContactForm/ContactForm';
import './EditContact.css';
import {editContact, editedContact} from "../../store/reducers/actions";

class EditContact extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: ''
    };

    componentDidMount() {
        console.log(this.props.editedContact);
        const id = this.props.match.params.id;
        this.props.editContact(id);
    }

    componentWillReceiveProps(nextProps) {
        const currentContact = {...nextProps.editedContact};
        this.setState({
            name: currentContact.name,
            phone: currentContact.phone,
            email: currentContact.email,
            photo: currentContact.photo
        });
    }

    changeValue = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    saveEditedContact = event => {
        event.preventDefault();
        const id = this.props.match.params.id;

        const contact = {...this.state};
        this.props.saveEditedContact(id, contact, this.props.history);
    };

    render() {
        let imagePreview = 'http://www.finescale.com/sitefiles/images/no-preview-available.png';

        if (this.state.photo !== '') {
            imagePreview = this.state.photo;
        }

        let contactForm = <ContactForm
            title="Edit contact"
            changeValue={this.changeValue}
            name={this.state.name}
            email={this.state.email}
            phone={this.state.phone}
            photo={this.state.photo}
            preview={imagePreview}
            saved={this.saveEditedContact}
        />;

        if(this.props.loading) {
            contactForm = <p style={{fontSize: '30px', textAlign: 'center'}}>Loading...</p>
        }

        return (
            <div className="edit-contact">
                {contactForm}
            </div>
        )
    }
}

const mapToProps = state => {
    return {
        loading: state.loading,
        editedContact: state.editedContact
    }
};

const mapDispatchToProps = dispatch => {
    return {
        editContact: (id) => dispatch(editContact(id)),
        saveEditedContact: (id, contact, history) => dispatch(editedContact(id, contact, history))
    }
};

export default connect(mapToProps, mapDispatchToProps)(EditContact);