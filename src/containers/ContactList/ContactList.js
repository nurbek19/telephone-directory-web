import React, {Component, Fragment} from 'react';
import {connect} from 'react-redux';

import Contact from '../../components/Contact/Contact';
import Modal from '../../components/UI/Modal/Modal';
import ModalContent from '../../components/ModalContent/ModalContent';
import './ContactList.css';
import {getContacts, removeContact, initPurchasable, reinitPurchasable} from "../../store/reducers/actions";

class ContactList extends Component {

    purchaseContinueHandler = () => {
        this.props.history.push(`/contact/${this.props.clickedContact.id}/edit`);
    };

    componentDidMount() {
        this.props.getContacts();
    }

    render() {
        const clickedContact = {...this.props.clickedContact};
        let listContacts = (this.props.contacts.map(contact => {
            return <Contact
                showed={() => this.props.showModal(contact)}
                key={contact.id}
                img={contact.photo}
                name={contact.name}
            />
        }));

        if(this.props.loading) {
            listContacts = <p style={{fontSize: '30px', textAlign: 'center'}}>Loading...</p>;
        }

        return (
            <Fragment>
                <Modal
                    show={this.props.purchasing}
                    closed={this.props.hideModal}
                >
                    <ModalContent
                        closed={this.props.hideModal}
                        img={clickedContact.photo}
                        name={clickedContact.name}
                        phone={clickedContact.phone}
                        email={clickedContact.email}
                        edit={this.purchaseContinueHandler}
                        remove={() => this.props.removeContact(clickedContact.id)}
                    />
                </Modal>

                <div className="contact-list">
                    {listContacts}
                </div>
            </Fragment>
        )
    }
}

const mapToProps = state => {
    return {
        contacts: state.contacts,
        loading: state.loading,
        purchasing: state.purchasing,
        clickedContact: state.clickedContact
    }
};

const mapDispatchToProps = dispatch => {
    return {
        getContacts: () => dispatch(getContacts()),
        removeContact: id => dispatch(removeContact(id)),
        showModal: contact => dispatch(initPurchasable(contact)),
        hideModal: () => dispatch(reinitPurchasable()),
    }
};

export default connect(mapToProps, mapDispatchToProps)(ContactList);