import React, {Component} from 'react';
import {connect} from 'react-redux';

import ContactForm from '../../components/ContactForm/ContactForm';
import './NewContact.css';
import {addNewContact} from "../../store/reducers/actions";

class NewContact extends Component {
    state = {
        name: '',
        phone: '',
        email: '',
        photo: ''
    };

    changeValue = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    saveNewContact = event => {
        event.preventDefault();
        const contact = {...this.state};
        this.props.addNewContact(contact, this.props.history);
    };

    render() {
        let imagePreview = 'http://www.finescale.com/sitefiles/images/no-preview-available.png';

        if (this.state.photo !== '') {
            imagePreview = this.state.photo;
        }

        let contactForm = <ContactForm
            title="Add new contact"
            changeValue={this.changeValue}
            name={this.state.name}
            email={this.state.email}
            phone={this.state.phone}
            photo={this.state.photo}
            preview={imagePreview}
            saved={this.saveNewContact}
        />;

        if(this.props.loading) {
            contactForm = <p style={{fontSize: '30px', textAlign: 'center'}}>Loading...</p>
        }

        return (
            <div className="new-contact">
               {contactForm}
            </div>
        )
    }
}

const mapToProps = state => {
    return {
        loading: state.loading
    }
};

const mapDispatchToProps = dispatch => {
    return {
        addNewContact: (contact, history) => dispatch(addNewContact(contact, history))
    }
};

export default connect(mapToProps, mapDispatchToProps)(NewContact);
