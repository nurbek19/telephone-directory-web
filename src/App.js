import React, {Component, Fragment} from 'react';
import {Route, Switch, Link} from 'react-router-dom';

import './App.css';
import ContactList from "./containers/ContactList/ContactList";
import NewContact from "./containers/NewContact/NewContact";
import EditContact from "./containers/EditContact/EditContact";

class App extends Component {
    render() {
        return (
            <Fragment>
                <header>
                    <Link to="/">Contacts</Link>
                    <Link to="/new-contact">Add new contact</Link>
                </header>
                <Switch>
                    <Route path="/" exact component={ContactList}/>
                    <Route path="/new-contact" component={NewContact}/>
                    <Route path="/contact/:id/edit" component={EditContact}/>
                </Switch>
            </Fragment>
        );
    }
}

export default App;
