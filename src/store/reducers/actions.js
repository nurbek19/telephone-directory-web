import axios from '../../axios-contacts';

import * as actionTypes from './actionTypes';

export const newContactRequest = () => {
    return {type: actionTypes.NEW_CONTACT_REQUEST}
};

export const newContactSuccess = () => {
    return {type: actionTypes.NEW_CONTACT_SUCCESS}
};

export const newContactError = () => {
    return {type: actionTypes.NEW_CONTACT_ERROR}
};

export const contactsRequest = () => {
    return {type: actionTypes.CONTACTS_REQUEST}
};

export const contactsSuccess = contacts => {
    return {type: actionTypes.CONTACTS_SUCCESS, contacts}
};

export const contactsError = () => {
    return {type: actionTypes.CONTACTS_ERROR}
};

export const removeContactRequest = () => {
    return {type: actionTypes.REMOVE_CONTACT_REQUEST}
};

export const removeContactSuccess = id => {
    return {type: actionTypes.REMOVE_CONTACT_SUCCESS, id}
};

export const removeContactError = () => {
    return {type: actionTypes.REMOVE_CONTACT_ERROR}
};

export const editContactRequest = () => {
    return {type: actionTypes.EDIT_CONTACT_REQUEST}
};

export const editContactSuccess = contact => {
    return {type: actionTypes.EDIT_CONTACT_SUCCESS, contact}
};

export const editContactError = () => {
    return {type: actionTypes.EDIT_CONTACT_ERROR}
};

export const editedContactRequest = () => {
    return {type: actionTypes.EDITED_CONTACT_REQUEST}
};

export const editedContactSuccess = () => {
    return {type: actionTypes.EDITED_CONTACT_SUCCESS}
};

export const editedContactError = () => {
    return {type: actionTypes.EDITED_CONTACT_ERROR}
};

export const initPurchasable = contact => {
    return {type: actionTypes.PURCHASABLE, contact};
};

export const reinitPurchasable = () => {
    return {type: actionTypes.NOT_PURCHASABLE};
};

export const addNewContact = (contact, history) => {
    return dispatch => {
        dispatch(newContactRequest());
        axios.post('/contacts.json', contact).then(() => {
            dispatch(newContactSuccess());
            history.push('/');
        }, error => {
            dispatch(newContactError());
        })
    }
};

export const getContacts = () => {
    return dispatch => {
        dispatch(contactsRequest());
        axios.get('/contacts.json').then(response => {
            dispatch(contactsSuccess(response.data));
        }, error => {
            dispatch(contactsError());
        })
    }
};

export const removeContact = id => {
  return dispatch => {
      dispatch(removeContactRequest());
      axios.delete(`/contacts/${id}.json`).then(() => {
          dispatch(removeContactSuccess(id));
      }, error => {
          dispatch(removeContactError());
      })
  }
};

export const editContact = id => {
    return dispatch => {
        dispatch(editContactRequest());
        axios.get(`/contacts/${id}.json`).then(response => {
            dispatch(editContactSuccess(response.data));
        }, error => {
            dispatch(editContactError());
        })
    }
};

export const editedContact = (id, contact, history) => {
    return dispatch => {
        dispatch(editedContactRequest());
        axios.put(`/contacts/${id}.json`, contact).then(() => {
            dispatch(editedContactSuccess());
            history.push('/');
        }, error => {
            dispatch(editedContactError());
        })
    }
};