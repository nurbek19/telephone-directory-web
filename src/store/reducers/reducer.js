import * as actionTypes from './actionTypes';

const initialState = {
    contacts: [],
    clickedContact: null,
    loading: false,
    purchasing: false,
    editedContact: null
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.NEW_CONTACT_REQUEST:
            return {...state, loading: true};
        case actionTypes.NEW_CONTACT_SUCCESS:
            return {...state, loading: false};
        case actionTypes.NEW_CONTACT_ERROR:
            return {...state, loading: false};
        case actionTypes.CONTACTS_REQUEST:
            return {...state, loading: true};
        case actionTypes.CONTACTS_SUCCESS:

            const contacts = [];
            for (let key in action.contacts) {
                contacts.unshift({...action.contacts[key], id: key})
            }

            return {...state, contacts, loading: false};
        case actionTypes.CONTACTS_ERROR:
            return {...state, loading: false};
        case actionTypes.REMOVE_CONTACT_REQUEST:
            return {...state, loading: true};
        case actionTypes.REMOVE_CONTACT_SUCCESS:
            let listContacts = [...state.contacts];
            const index = listContacts.findIndex(contact => contact.id === action.id);
            listContacts.splice(index, 1);
            return {...state, contacts: listContacts, loading: false, purchasing: false};
        case actionTypes.REMOVE_CONTACT_ERROR:
            return {...state, loading: false};
        case actionTypes.PURCHASABLE:
            return {...state, purchasing: true, clickedContact: action.contact};
        case actionTypes.NOT_PURCHASABLE:
            return {...state, purchasing: false, clickedContact: null};
        case actionTypes.EDIT_CONTACT_SUCCESS:
            return {...state, editedContact: action.contact};
        case actionTypes.EDITED_CONTACT_SUCCESS:
            return {...state, purchasing:false};
        default:
            return state;
    }
};

export default reducer;
